
public class Location extends SpacePlace {
  public int a;
  public int z;
  public DIRECTION d;
  
  public enum DIRECTION {VERTICAL, HORIZONTAL};
  
  public Location(int r, int c) {
    this.z = r;
    this.a = c;
  }

  public Location(int r, int c, DIRECTION d) {    
    this(r,c);
    this.d=d;
  }
  
  public String toString() {
    if(d==null){
      return "(" + (a+1) + "," + (z+1) + ")";
    } else {
      return "(" + (a+1) + "," + (z+1) + "," + d + ")";
    }
  }
}
